# **darkLibrary - Version 1.0**

Library made for simplifing things.

**Changelog:**

***1.0:***
* Save and load JSON by an array of class's instances;
* Function for getting a file into url's directory.

-------------------------------------------------

Una libreria realizzata per semplificare le cose.

**Cambiamenti:**

***1.0:***
* Salvataggio e caricamento in JSON tramite un'array di istanze di una classe;
* Funzione che permette di ottenere un file all'interno della directory dei file.


