import XCTest

import darkLibraryTests

var tests = [XCTestCaseEntry]()
tests += darkLibraryTests.allTests()
XCTMain(tests)
