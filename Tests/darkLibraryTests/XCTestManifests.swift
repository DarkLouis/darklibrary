import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(darkLibraryTests.allTests),
    ]
}
#endif
