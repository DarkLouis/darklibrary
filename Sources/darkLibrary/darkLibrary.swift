/*
    darkLibrary
    made by DarkLouis - https://gitlab.com/DarkLouis
*/

import Foundation

extension URL    
{
    public func isValidFile(debug: Bool = false) -> Bool 
    {
        let path = self.path
        if (FileManager.default.fileExists(atPath: path))   
        {
            if(debug == true) { print("File is valid!") }
            return true
        }
        else        
        {
            if(debug == true) { print("File is not valid!") }
            return false;
        }
    }
}

public func getDirectoryUrl(file: String) -> URL
{
    let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    return URL(string: "\(documentDirectoryUrl)/\(file)")!
}

public func getBundleUrl(name: String, ext: String, subDirectory: String = "") -> URL?
{
    if let url = Bundle.main.url(forResource: name, withExtension: ext, subdirectory: subDirectory)
    {
        return url
    }
    
    return nil
}

public func saveJSON<T : Encodable>(_ dataList: T, into: URL, debug: Bool = false)
{
    do
    {
        let e = JSONEncoder()
        e.outputFormatting = .prettyPrinted
        let data = try! e.encode(dataList)
        try data.write(to: into, options: [])
        if(debug == true)
        {
            print(String(data: data, encoding: .utf8)!)
            print("URL: \(into)")
            print("Save complete.")
        }
    }
    catch
    {
        print(error)
    }
}

public func loadJSON<T : Decodable>(_ classList: inout T, from: URL, debug: Bool = false)
{
    do
    {
        let data = try Data(contentsOf: from, options: [])
        classList = try JSONDecoder().decode(type(of: classList).self, from: data)
        if(debug == true)
        {
            dump(classList)
            print("Data(s) loaded successfully")
        }
    }
    catch
    {
        print(error)
    }
}
